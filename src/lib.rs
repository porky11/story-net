use pns::{Net, SafeNet};

use std::cmp::Ordering;

pub struct StoryNet(Net);

fn set_differences(a: &[u32], b: &[u32]) -> (bool, bool) {
    let mut i = 0;
    let mut j = 0;
    let mut first = false;
    let mut second = false;

    loop {
        match (i < a.len(), j < b.len()) {
            (false, false) => return (first, second),
            (true, false) => return (true, second),
            (false, true) => return (first, true),
            (true, true) => match a[i].cmp(&b[j]) {
                Ordering::Less => {
                    if second {
                        return (true, true);
                    }

                    first = true;
                    i += 1;
                }
                Ordering::Greater => {
                    if first {
                        return (true, true);
                    }

                    second = true;
                    j += 1;
                }
                Ordering::Equal => {
                    i += 1;
                    j += 1;
                }
            },
        }
    }
}

fn set_equal(a: &[u32], b: &[u32]) -> bool {
    let count = a.len();
    if count != b.len() {
        return false;
    }

    for i in 0..count {
        if a[i] != b[i] {
            return false;
        }
    }

    true
}

impl StoryNet {
    pub fn new(net: Net) -> Self {
        Self(net)
    }

    pub fn add_scene(&mut self) -> u32 {
        let place = self.0.add_place();
        self.0.add_connected_transition(&[place])
    }

    fn join_dependency_places(&mut self, index: u32) -> bool {
        let transitions = self.0.transitions();

        let node = &transitions[index as usize];
        let prev_places = node.prev();

        let places = self.0.places();

        for (i, &place) in prev_places.iter().enumerate() {
            let place_node = &places[place as usize];
            for &other in &prev_places[(i + 1)..] {
                let other_node = &places[other as usize];
                if set_equal(place_node.next(), other_node.next()) {
                    let (place_additions, other_additions) =
                        set_differences(place_node.prev(), other_node.prev());
                    if !other_additions {
                        self.0.remove_place(place);
                        return true;
                    } else if !place_additions {
                        self.0.remove_place(other);
                        return true;
                    }
                }
            }
        }

        false
    }

    fn join_parallel_places(&mut self, index: u32) -> bool {
        let transitions = self.0.transitions();

        let node = &transitions[index as usize];
        let next_places = node.next();

        let places = self.0.places();

        for (i, &place) in next_places.iter().enumerate() {
            let place_node = &places[place as usize];
            for &other in &next_places[(i + 1)..] {
                let other_node = &places[other as usize];
                if set_equal(place_node.prev(), other_node.prev()) {
                    let (place_additions, other_additions) =
                        set_differences(place_node.next(), other_node.next());
                    if place_additions != other_additions {
                        if place_additions {
                            self.0.remove_place(other);
                        } else {
                            self.0.remove_place(place);
                        }
                        return true;
                    }
                }
            }
        }

        false
    }

    pub fn add_path(&mut self, new: u32, dependencies: &[u32]) {
        let transitions = self.0.transitions();

        let new_node = &transitions[new as usize];
        let prev_places: Vec<_> = new_node.prev().into_iter().cloned().collect();

        for &dependency in dependencies {
            let transitions = self.0.transitions();

            let dependency_node = &transitions[dependency as usize];
            let next_places: Vec<_> = dependency_node.next().into_iter().cloned().collect();
            if next_places.is_empty() {
                for &place in &prev_places {
                    let place_clone = self.0.duplicate_place(place);
                    self.0.connect_out(dependency, place_clone);
                }
            } else {
                for &place in &next_places {
                    let place_clone = self.0.duplicate_place(place);
                    self.0.connect_in(new, place_clone);
                }
            }
        }

        for place in prev_places {
            self.0.remove_place(place);
        }

        for &dependency in dependencies {
            while self.join_parallel_places(dependency) {}
        }

        while self.join_dependency_places(new) {}
    }

    pub fn make_parallel(&mut self, dependency: u32, parallels: &[u32]) {
        let transitions = self.0.transitions();

        let dependency_node = &transitions[dependency as usize];

        let next_places: Vec<_> = dependency_node.next().iter().cloned().collect();

        for place in next_places {
            let places = self.0.places();
            let place_node = &places[place as usize];

            let mut parallel_transitions = Vec::new();
            for &transition in place_node.next() {
                if parallels.contains(&transition) {
                    parallel_transitions.push(transition);
                }
            }

            for &transition in &parallel_transitions {
                let place_clone = self.0.duplicate_place(place);
                for &parallel in &parallel_transitions {
                    if parallel != transition {
                        self.0.disconnect_in(parallel, place_clone);
                    }
                }
                self.0.disconnect_in(transition, place);
            }

            let places = self.0.places();
            let place_node = &places[place as usize];
            if place_node.next_count == 0 {
                self.0.remove_place(place);
            }
        }

        while self.join_parallel_places(dependency) {}
    }
}
